from PyQt5.QtCore import QThread, pyqtSignal, QRunnable

from download_thread_signals import DownloadThreadSignals

class DownloadThread(QRunnable):
    """
    Runs a download thread.
    """

    def __init__(self, chapter_name, chapter_url, loader):
        self.chapter_name = chapter_name
        self.chapter_url = chapter_url
        self.loader = loader
        self.signals = DownloadThreadSignals()
        super().__init__()
    
    def run(self):
        img_list = []
        err = []
        try:
            img_list, err = self.loader.downloadMangaChapter(self.chapter_url)
            if(len(err)==0):
                self.signals.result.emit(self.chapter_name, img_list)
            else:
                self.signals.error.emit(self.chapter_name, err)
        except:
            self.signals.error.emit(self.chapter_name, err)
        finally:
            self.signals.finished.emit()
        
