from PyQt5 import QtCore, QtWidgets, QtGui

from gui.Ui_settingsUi import Ui_SettingsUi
import signal


class SettingsUi(QtWidgets.QMainWindow):

    def __init__(self, settings ,parent=None):
        super(SettingsUi, self).__init__(parent)
        self.ui = Ui_SettingsUi()
        self.ui.setupUi(self)
        self.settings = settings
        self.mainUi = parent
        self.ui.outDirectoryLineEdit.setText(self.settings["save_dir"])
        self.ui.threadSpinBox.setValue(self.settings["thread_count"])
        self.ui.qualitySpinBox.setValue(self.settings["quality"])
        self.ui.showSavedCheckBox.setChecked(self.settings["show_saved"])
        self.deleteRead = False
        self.ui.deleteInfoLabel.setVisible(False)
        self.initSlot()

    def initSlot(self):
        self.ui.saveSettingsButton.clicked.connect(self.saveSettings)
        self.ui.cancelSettingsButton.clicked.connect(self.close)
        self.ui.outDirectoryToolButton.clicked.connect(self.openFileNameDialog)
        self.ui.deleteReadButton.clicked.connect(self.showDeleteDialog)
        
    def saveSettings(self):
        print(" settings saved")
        self.settings["save_dir"] = self.ui.outDirectoryLineEdit.text()
        self.settings["thread_count"] = self.ui.threadSpinBox.value()
        self.settings["quality"] = self.ui.qualitySpinBox.value()
        self.settings["show_saved"] = self.ui.showSavedCheckBox.isChecked()
        if(self.deleteRead):
            self.settings["read_manga"] = []
        self.mainUi.writeSettingsToFile()
        self.close()

    def openFileNameDialog(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        dirPath = QtWidgets.QFileDialog.getExistingDirectory(self, "Select output directory", options=options)
        if dirPath:
            self.ui.outDirectoryLineEdit.setText(dirPath)

    def showDeleteDialog(self):
        msgBox = QtWidgets.QMessageBox()
        msgBox.setIcon(QtWidgets.QMessageBox.Information)
        msgBox.setText("Do you want to delete the save of all read mangas? \n(This does not delete the Files)")
        msgBox.setWindowTitle("Settings - Delete Read")
        msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)

        returnValue = msgBox.exec()
        if returnValue == QtWidgets.QMessageBox.Ok:
            self.deleteRead = True
            self.ui.deleteInfoLabel.setVisible(True)
