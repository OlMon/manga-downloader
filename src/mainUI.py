from PyQt5 import QtCore, QtWidgets

from gui.Ui_mainUi import Ui_MangaDL
from settingsUi import SettingsUi
import loader
from download_thread import DownloadThread
from new_chapter_count_thread import NewChapterCountThread

import json
import os
import signal
import string

class MainUi(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        super(MainUi, self).__init__(parent)
        self.ui = Ui_MangaDL()
        self.ui.setupUi(self)

        self.current_dir = os.path.join(
            os.path.dirname(os.path.abspath(__file__)))
        self.settings = {}

        self.initSettings()

        self.ui.savedMangaGroupBox.setChecked(self.settings["show_saved"])
        self.ui.savedMangasFrame.setVisible(
            self.ui.savedMangaGroupBox.isChecked())

        if not os.path.exists(self.settings["save_dir"]):
            os.makedirs(self.settings["save_dir"])

        self.chapter_list = []
        self.thread_pool = QtCore.QThreadPool()
        self.ui.progressBar.setVisible(False)

        self.initSlot()

        signal.signal(signal.SIGINT, self.close)
        signal.signal(signal.SIGTERM, self.close)

        self.ui.searchTableWidget.setHorizontalHeaderLabels(
            ['Chapter', 'Url', 'New?'])
        self.ui.savedMangaTableWidget.setHorizontalHeaderLabels(
            ['Manga', 'Url', '# New Chapters'])

        self.displaySaved()
        #DEBUG
        self.ui.urlLineEdit.setText(
           "https://www.mangatown.com/manga/mahou_shoujo_of_the_end/")

    def initSlot(self):
        self.ui.loadUrlButton.clicked.connect(self.loadUrl)
        self.ui.savedMangaGroupBox.clicked.connect(self.toggleSavedGroupBox)
        self.ui.downloadButton.clicked.connect(self.downloadSelectedChapters)
        self.ui.markReadButton.clicked.connect(self.markChaptersAsRead)
        self.ui.saveMangaButton.clicked.connect(self.saveToFavorit)
        self.ui.updateSavedButton.clicked.connect(self.displaySaved)
        self.ui.loadMangaButton.clicked.connect(self.loadSaved)
        self.ui.removeMangaButton.clicked.connect(self.removeSaved)
        self.ui.savedMangaTableWidget.doubleClicked.connect(self.loadSaved)
        self.ui.actionAbout.triggered.connect(self.displayAbout)
        self.ui.actionExit.triggered.connect(self.close)
        self.ui.actionSettings.triggered.connect(self.showSettings)

    def initSettings(self):
        if not os.path.exists(self.current_dir + os.path.sep + "settings.json"):
            self.settings["quality"] = 75
            self.settings["read_manga"] = {}
            self.settings["saved_mangas"] = []
            self.settings["show_saved"] = True
            self.settings["save_dir"] = self.current_dir + os.path.sep + "output" + os.path.sep
            self.settings["thread_count"] = 3
            with open(self.current_dir + os.path.sep + "settings.json", "w") as out_f:
                json.dump(self.settings, out_f)
        else:
            with open(self.current_dir + os.path.sep + "settings.json") as in_f:
                self.settings = json.load(in_f)

    def showSettings(self):
        settingUi = SettingsUi(self.settings,self)
        settingUi.show()

    def saveToFavorit(self):
        if((self.manga_name != "") and (self.ui.urlLineEdit.text() != "")):
            print(self.settings["saved_mangas"])
            print((self.manga_name, self.ui.urlLineEdit.text()))
            if([self.manga_name, self.ui.urlLineEdit.text()] not in
               self.settings["saved_mangas"]):
                print(type(self.ui.urlLineEdit.text()))
                self.settings["saved_mangas"].append(
                    (self.manga_name, self.ui.urlLineEdit.text()))
                if(not (self.manga_name in self.settings["read_manga"])):
                    self.settings["read_manga"][self.manga_name] = []
                self.displaySaved()
                self.writeSettingsToFile()

    def displaySaved(self):
        self.ui.savedMangaTableWidget.setRowCount(0)
        for i in range(len(self.settings["saved_mangas"])):
            rowPos = self.ui.savedMangaTableWidget.rowCount()
            self.ui.savedMangaTableWidget.insertRow(rowPos)
            self.ui.savedMangaTableWidget.setItem(
                rowPos, 0,
                QtWidgets.QTableWidgetItem(self.settings["saved_mangas"][i][0]))
            self.ui.savedMangaTableWidget.setItem(
                rowPos, 1,
                QtWidgets.QTableWidgetItem(self.settings["saved_mangas"][i][1]))
            self.ui.savedMangaTableWidget.setItem(
                rowPos, 2,
                QtWidgets.QTableWidgetItem("loading..."))
            self.updateNewChapters(i, rowPos)

    def updateNewChapters(self, row_id, row_pos):
        loader = self.getLoader(self.settings["saved_mangas"][row_id][1])
        if(loader):
            counter_thread = NewChapterCountThread(row_pos,
                                                   self.settings["saved_mangas"][row_id][1],
                                                   loader,
                                                   self.settings["read_manga"][self.settings["saved_mangas"][row_id][0]])
            counter_thread.signals.result.connect(self.fillNewChapter)
            self.thread_pool.start(counter_thread)
        else:
            self.ui.savedMangaTableWidget.setItem(
                row_pos, 2, QtWidgets.QTableWidgetItem("ERROR"))
            self.ui.statusbar.showMessage("There was in error loading manga in line %i"%(row_pos+1), 3000)

    def fillNewChapter(self, row_pos, new_chapters):
        self.ui.savedMangaTableWidget.setItem(
                row_pos, 2, QtWidgets.QTableWidgetItem(str(new_chapters)))

    def removeSaved(self):
        del self.settings["saved_mangas"][self.ui.savedMangaTableWidget.currentRow()]
        self.writeSettingsToFile()
        self.displaySaved()


    def loadSaved(self):
        self.ui.urlLineEdit.setText(
            self.ui.savedMangaTableWidget.item(
                self.ui.savedMangaTableWidget.currentRow(), 1).text())
        self.loadUrl()

    def convertChapterNameToFileName(self, chapter_name):
        valid_chars = "-_.() %s%s"%(string.ascii_letters, string.digits)
        file_name = ''.join(c for c in chapter_name if c in valid_chars)
        return file_name

    def handleDownloadError(self, chapter_name, error_list):
        self.ui.progressBar.setValue(self.ui.progressBar.value() + 1)
        for i in range(self.ui.searchTableWidget.rowCount()):
            if(self.ui.searchTableWidget.item(i, 0).text() == chapter_name):
                self.ui.searchTableWidget.setItem(i, 2 ,QtWidgets.QTableWidgetItem("Error Downloading!"))
                break  # Don't Search further if found proper row
        if(len(error_list) > 0):
            self.displayError("Download Error", "TODO: Write proper error")
        if(self.thread_pool.activeThreadCount() == 0):
            self.setAllInteractionsEnabled(True)
            self.writeSettingsToFile()
    
    def saveToPDF(self, chapter_name, img_list):
        if(not (self.manga_name in self.settings["read_manga"])):
            self.settings["read_manga"][self.manga_name] = []
        self.settings["read_manga"][self.manga_name].append(chapter_name)
        saveDir = self.settings["save_dir"] + self.manga_name + os.path.sep
        if not os.path.exists(saveDir):
            os.makedirs(saveDir)
        file_name = self.convertChapterNameToFileName(chapter_name)
        img_list[0].save(saveDir + file_name + ".pdf", "PDF", optimize=True,
                         quality=self.settings["quality"], resolution=100.0, save_all=True,
                         append_images=img_list[1:])
        self.ui.progressBar.setValue(self.ui.progressBar.value() + 1)
        for i in range(self.ui.searchTableWidget.rowCount()):
            if(self.ui.searchTableWidget.item(i, 0).text() == chapter_name):
                self.ui.searchTableWidget.setItem(i, 2 ,QtWidgets.QTableWidgetItem("Finished!"))
                break  # Don't Search further if found proper row
        if(self.thread_pool.activeThreadCount() == 0):
            self.setAllInteractionsEnabled(True)
            self.writeSettingsToFile()

    def downloadSelectedChapters(self):
        self.ui.progressBar.setVisible(True)
        self.ui.progressBar.setValue(0)
        self.ui.progressBar.setMaximum(
            len(self.ui.searchTableWidget.selectionModel().selectedRows()))
        self.thread_pool.setMaxThreadCount(self.settings["thread_count"])
        self.setAllInteractionsEnabled(False)
        for i in self.ui.searchTableWidget.selectionModel().selectedRows():
            self.ui.searchTableWidget.setItem(i.row(), 2,QtWidgets.QTableWidgetItem("Downloading..."))
            downloader = DownloadThread(
                self.chapter_list[i.row()][0], self.chapter_list[i.row()][1],
                self.loader)
            downloader.signals.result.connect(self.saveToPDF)
            downloader.signals.error.connect(self.handleDownloadError)
            self.thread_pool.start(downloader)

    def toggleSavedGroupBox(self):
        self.ui.savedMangasFrame.setVisible(
            self.ui.savedMangaGroupBox.isChecked())

    def displayAbout(self):
        sites_str = loader.getLoaderString()
        msg = QtWidgets.QMessageBox()
        #msg.setIcon(QtWidgets.QMessageBox.Default)
        msg.setText("Manga DL by OlMon")
        msg.setInformativeText("Supported Sites:\n" + '\n'.join(sites_str))
        msg.setWindowTitle("About")
        msg.exec_()
        
    def displayError(self, error_title, error_msg):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText(error_title)
        msg.setInformativeText(error_msg)
        msg.setWindowTitle("Error")
        msg.exec_()

    def setAllInteractionsEnabled(self, enable):
        self.ui.loadUrlButton.setEnabled(enable)
        self.ui.savedMangaButtonFrame.setEnabled(enable)
        self.ui.savedMangasFrame.setEnabled(enable)
        self.ui.resultFrame.setEnabled(enable)
        self.ui.urlLineEdit.setEnabled(enable)
        self.ui.actionSettings.setEnabled(enable)
        
    def getLoader(self, url):
        return loader.getLoader(url)

    def loadUrl(self):
        self.ui.progressBar.setVisible(False)
        self.loader = self.getLoader(self.ui.urlLineEdit.text())
        if(loader):
            self.manga_name = self.loader.getMangaName()
            self.setAllInteractionsEnabled(False)
            self.displayChapters(self.loader.getListOfChapters())
            self.setAllInteractionsEnabled(True)
        else:
            self.displayError("Url not supported!", "No proper loader found.")
            self.manga_name = ""
            self.chapter_list = []
            self.ui.searchTableWidget.setRowCount(0)

    def displayChapters(self, chapter_list):
        self.chapter_list = chapter_list
        self.ui.searchTableWidget.setRowCount(0)
        for i in range(len(chapter_list)):
            rowPos = self.ui.searchTableWidget.rowCount()
            self.ui.searchTableWidget.insertRow(rowPos)
            self.ui.searchTableWidget.setItem(
                rowPos, 0, QtWidgets.QTableWidgetItem(chapter_list[i][0]))
            self.ui.searchTableWidget.setItem(
                rowPos, 1, QtWidgets.QTableWidgetItem(chapter_list[i][1]))
            if(self.manga_name in self.settings["read_manga"]):
                if(chapter_list[i][0] not in self.settings["read_manga"][self.manga_name]):
                    self.ui.searchTableWidget.setItem(
                        rowPos, 2, QtWidgets.QTableWidgetItem("New"))
                else:
                    self.ui.searchTableWidget.setItem(
                        rowPos, 2, QtWidgets.QTableWidgetItem(""))
            else:
                self.ui.searchTableWidget.setItem(
                    rowPos, 2, QtWidgets.QTableWidgetItem("New"))

    def writeSettingsToFile(self):
        print("main Saved")
        with open(self.current_dir + os.path.sep + "settings.json", "w") as out_f:
            json.dump(self.settings, out_f)

    def markChaptersAsRead(self):
        for i in self.ui.searchTableWidget.selectionModel().selectedRows():
            if(not(self.manga_name in self.settings["read_manga"])):
                self.settings["read_manga"][self.manga_name] = []
            self.settings["read_manga"][self.manga_name].append(self.chapter_list[i.row()][0])
        self.displayChapters(self.chapter_list)
        self.writeSettingsToFile()
