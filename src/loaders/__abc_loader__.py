from abc import ABC, abstractmethod

class ABC_Loader(ABC):
    """Abstract base class for all loaders for specific sites. 
       Every method is needed to be implemented.
    """
 
    def __init__(self, manga_url):
        """Constructor.

        Arguments:
        - `manga_url`: The url of the manga to load with the loader        
        """
        self.manga_url = manga_url
        super().__init__()

    @abstractmethod
    def __str__(self):
        """ Nice string representation like: "NameOfSite: UrlToSite"
        """
        pass
        
    @abstractmethod
    def isValidUrl(self):
        """Method to check if the provided url fromt he constructor is a valid url for this class.
        """
        pass

    @abstractmethod
    def getMangaName(self):
        """Method to get the manga name from the loaded chapter
        """
        pass

    @abstractmethod
    def getListOfChapters(self):
        """Method to get all chapters in this manga. The format is an array of tupels
           arr[id] = (chaptername, url)

        Returns:
        - `result`: List of chapters (arr[id] = (chaptername, url))
        """
        pass

    @abstractmethod
    def downloadMangaChapter(self, chapter_url):
        """Method to download a all images for a chapter.
    
        Arguments:
        - `chapter_url`: The url of the given Chapter (str)
        
        Return:
        - `result`: List of PIL images, list of tuples on which url which element 
        could not be downloaded
        """
        pass
