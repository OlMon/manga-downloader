from urllib.request import urlopen, Request
from urllib.error import HTTPError
from bs4 import BeautifulSoup
from PIL import Image
from io import BytesIO


#from .__abc_loader__ import ABC_Loader
from __abc_loader__ import ABC_Loader

class MangaGo_Loader(ABC_Loader):

    def __str__(self):
        return "MangaGo: www.mangago.me"
    
    def getHtmlSoup(self, url):
        req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
        response = urlopen(req).read()

        str_response = response.decode("utf8")

        return BeautifulSoup(str_response, 'html.parser')

    def isValidUrl(self):
        return "mangago" in self.manga_url

    def getMangaName(self):
        return self.getHtmlSoup(self.manga_url).find('h1').string

    def getListOfChapters(self):
        soup = self.getHtmlSoup(self.manga_url)
        chapter_list = []
        for li in soup.select('#chapter_table'):
            for a in li.find_all('a', 'chico'):
                chapter_list.append((a.find('b').string.strip(), a.get('href').strip()))

        return chapter_list

    def downloadMangaChapter(self, chapter_url, err_tries=10):
        err_report = []
        img_list = []
        soup = self.getHtmlSoup(chapter_url)
        for div in soup.select('#pagenavigation', limit=1):
            print(div)
            for option in div.find_all('a'):
                #if('featured' not in option.get('value')):
                singel_page_soup = self.getHtmlSoup(
                    'https://www.mangago.me'+option.get('href'))


                img_url = singel_page_soup.find(id='pic_container').find_all('a', style="").get('src')
                print(img_url)
            #     img_request = Request(
            #         img_url,
            #         headers={'User-Agent': 'Mozilla/5.0',
            #                  'Referer': 'https://www.mangago.me/'})
            #     success = False
            #     for err in range(err_tries):
            #         try:
            #             img_request = urlopen(img_request).read()
            #             success = True
            #             break
            #         except HTTPError:
            #             pass
            #     if(not success):
            #         err_report.append(
            #             ('https://www.mangatown.com'+option.get('value'),
            #              'https:'+img_url))
            #         print("got error")
            #     else:
            #         img = Image.open(BytesIO(img_request))
            #         if img.mode != "RGB":
            #             img = img.convert("RGB")
            #         img_list.append(img)
            #         print("got image")
            # return img_list, err_report
