from urllib.request import urlopen, Request
from urllib.error import HTTPError
from bs4 import BeautifulSoup
from PIL import Image
from io import BytesIO


from .__abc_loader__ import ABC_Loader


class MangaTown_Loader(ABC_Loader):

    def __str__(self):
        return "Mangapanda: www.mangapanda.com"
    
    def getHtmlSoup(self, url):
        req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
        response = urlopen(req).read()

        str_response = response.decode("utf8")

        return BeautifulSoup(str_response, 'html.parser')

    def isValidUrl(self):
        return "mangapanda" in self.manga_url

    def getMangaName(self):
        return self.getHtmlSoup(self.manga_url).find('h2').string

    def getListOfChapters(self):
        soup = self.getHtmlSoup(self.manga_url)
        chapter_list = []
        for tr in soup.find(id='listing').find_all('tr'):
            for a in tr.find_all('a'):
                chapter_list.append((a.string.strip(), a.get('href').strip()))

        return chapter_list

    def downloadMangaChapter(self, chapter_url, err_tries=10):
        err_report = []
        img_list = []
        soup = self.getHtmlSoup('http://www.mangapanda.com' + chapter_url)
        for div in soup.select('#selectpage', limit=1):
            for option in div.find('select').find_all('option'):
                print(option)
                if('featured' not in option.get('value')):
                    singel_page_soup = self.getHtmlSoup(
                        'http://www.mangapanda.com'+option.get('value'))

                    img_url = singel_page_soup.find(id="img").get('src')
                    img_request = Request(
                        img_url,
                        headers={'User-Agent': 'Mozilla/5.0'})
                    success = False
                    for err in range(err_tries):
                        try:
                            img_request = urlopen(img_request).read()
                            success = True
                            break
                        except HTTPError:
                            pass
                    if(not success):
                        err_report.append(
                            ('http://www.mangapanda.com'+option.get('value'),
                             img_url))
                        print("got error")
                    else:
                        img = Image.open(BytesIO(img_request))
                        if img.mode != "RGB":
                            img = img.convert("RGB")
                        img_list.append(img)
                        print("got image")
            return img_list, err_report
