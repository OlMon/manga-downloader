from urllib.request import urlopen, Request
from urllib.error import HTTPError
from bs4 import BeautifulSoup
from PIL import Image
from io import BytesIO


from .__abc_loader__ import ABC_Loader


class Mangakakalot_Loader(ABC_Loader):

    def __str__(self):
        return "Mangakakalot: www.mangakakalot.com"
    
    def getHtmlSoup(self, url):
        req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
        response = urlopen(req).read()

        str_response = response.decode("utf8")

        return BeautifulSoup(str_response, 'html.parser')

    def isValidUrl(self):
        return "mangakakalot" in self.manga_url

    def getMangaName(self):
        return self.getHtmlSoup(self.manga_url).find('h1').string

    def getListOfChapters(self):
        soup = self.getHtmlSoup(self.manga_url)
        chapter_list = []
        for div in soup.select('.chapter-list'):
            for a in div.find_all('a'):
                chapter_list.append((a.string.strip(), a.get('href').strip()))

        return chapter_list

    def downloadMangaChapter(self, chapter_url, err_tries=10):
        err_report = []
        img_list = []
        soup = self.getHtmlSoup(chapter_url)
        for img in soup.find(id='vungdoc').find_all('img'):
            img_url = img.get('src')
            print(img_url)
            img_request = Request(
                img_url,
                headers={'User-Agent': 'Mozilla/5.0', 'Referer': 'https://www.mangakakalot.com/'})
            success = False
            for err in range(err_tries):
                try:
                    img_request = urlopen(img_request).read()
                    success = True
                    break
                except HTTPError:
                    pass
            if(not success):
                err_report.append(
                    (chapter_url,
                     img_url))
                print("got error")
            else:
                img = Image.open(BytesIO(img_request))
                img_list.append(img)
                print("got image")
        return img_list, err_report
