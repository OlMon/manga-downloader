#from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5.QtWidgets import QMainWindow
from PyQt5 import QtCore, QtGui
from PyQt5 import QtWidgets
# from PyQt5.QtGui import QPalette, QColor
# from PyQt5.QtCore import Qt
from mainUI import MainUi

import sys

# Imports used loaders, need to loaded here, so it will be packaged
# with pyinstall
from urllib.request import urlopen, Request
from urllib.error import HTTPError
from bs4 import BeautifulSoup
from PIL import Image
from io import BytesIO


if __name__ == '__main__':
    #appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext
    app = QtWidgets.QApplication(sys.argv)
    # palette = QPalette()
    # palette.setColor(QPalette.Window, QColor(53, 53, 53))
    # palette.setColor(QPalette.WindowText, Qt.white)
    # appctxt.app.setPalette(palette)
    w = MainUi()
    w.show()
    exit_code = app.exec()      # 2. Invoke appctxt.app.exec_()
    sys.exit(exit_code)
