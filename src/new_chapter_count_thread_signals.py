from PyQt5.QtCore import QObject, pyqtSignal

class NewChapterCountThreadSignals(QObject):
    """
    Signal for the new chapter counter thread
    """
    finished = pyqtSignal()
    error = pyqtSignal()
    result = pyqtSignal(int, int)
