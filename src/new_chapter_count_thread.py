from PyQt5.QtCore import QThread, pyqtSignal, QRunnable

from new_chapter_count_thread_signals import NewChapterCountThreadSignals

class NewChapterCountThread(QRunnable):
    """
    Runs a new chapter counter thread.
    """

    def __init__(self, row_id, manga_url, loader, read_mangas):
        self.row_id = row_id
        self.manga_url = manga_url
        self.loader = loader
        self.read_mangas = read_mangas
        self.signals = NewChapterCountThreadSignals()
        super().__init__()

    def run(self):
        chapter_list = self.loader.getListOfChapters()
        chapters = [x[0] for x in chapter_list]
        new_chapters = len(set(chapters).difference(self.read_mangas))
        self.signals.result.emit(self.row_id, new_chapters)
        self.signals.finished.emit()
