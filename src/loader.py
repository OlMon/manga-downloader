import glob
import importlib
import inspect
import os


def getLoader(url):
    modul_name = 'loaders'
    current_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)))
    for file in glob.glob(current_dir + "/" + modul_name + "/" + "/*.py"):
        name = os.path.splitext(os.path.basename(file))[0]
        # Ignore __ files
        if name.startswith("__") or ("ABC" in name):
            continue
        module = importlib.import_module(modul_name+'.'+name)
        for member in dir(module):
            if not("ABC" in member) and ("_Loader" in member):
                handler_class = getattr(module, member)
                if handler_class and inspect.isclass(handler_class):
                    o = handler_class(url)
                    if o.isValidUrl():
                        return o

def getLoaderString():
    result = []
    modul_name = 'loaders'
    current_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)))
    for file in glob.glob(current_dir + "/" + modul_name + "/" + "/*.py"):
        name = os.path.splitext(os.path.basename(file))[0]
        # Ignore __ files
        if name.startswith("__") or ("ABC" in name):
            continue
        module = importlib.import_module(modul_name+'.'+name)
        for member in dir(module):
            if not("ABC" in member) and ("_Loader" in member):
                handler_class = getattr(module, member)
                if handler_class and inspect.isclass(handler_class):
                    o = handler_class("")
                    result.append(str(o))
    return result
