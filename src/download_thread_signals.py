from PyQt5.QtCore import QObject, pyqtSignal

class DownloadThreadSignals(QObject):
    """
    Signal for the download thread
    """
    finished = pyqtSignal()
    error = pyqtSignal(str ,list)
    result = pyqtSignal(str, list)
